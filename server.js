process.env.NODE_ENV !== "production" && require("dotenv").config();

const express = require("express");
const app = express();
const axios = require("axios").default;

const PORT = process.env.PORT || 3001;

app.get("/", async (req, res) => {
  const posts = await axios.get("https://jsonplaceholder.typicode.com/posts");
  return res.json(posts.data);
});

app.get("/apiKey", (req, res) => {
  res.json({
    apiKey: process.env.API_KEY,
  });
});

app.listen(PORT, console.log(`Server started at PORT ${PORT}`));
